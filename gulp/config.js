var dest = "./web";

var src = './src';

module.exports = {
  dest: dest,
  browserSync: {
    watchTask: true,
    proxy: "looker.front",
    notify: false

  },
  sass: {
    src: [src + "/sass/**/*.scss",src + "/javascript/views/**/*.scss"],
    dest: dest + "/assets/css"
  },
  datas: {
    src: [src + "/assets/datas/*.json"],
    dest: dest + "/assets/datas/"
  },
  fonts: {
    src: [src + "/assets/fonts/*"],
    dest: dest + "/assets/fonts/"
  },
  images: {
    src: src + "/assets/images/**",
    dest: dest + "/assets/images"
  },
  uglify: {
    src: src + "/js/app.js",
    dest: dest + "/assets/js"
  },
  iconfont: {
    src: src + "/svg/*.svg",
    path: src + "/css/templates/_icons.scss",
    targetPath: "../../../src/sass/_mixins/_glyphicons.scss", //PATH FROM DEST (WEB>ASSETS>FONT)
    fontPath: "../fonts/",
    dest: src + "/assets/fonts",
    fontName: "glyphicons"
  },
  markup: {
    src: [src + "/htdocs/**",src + "/htdocs/.htaccess"],
    dest: dest
  },
  browserify: {
    // Enable source maps
    debug: true,

    fullPaths: true,

    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: './src/javascript/index.js',
      dest: src+'/js',
      outputName: 'app.js'
    }]
  }
};
