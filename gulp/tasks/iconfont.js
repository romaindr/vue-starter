var gulp      = require('gulp'),
  iconfont    = require('gulp-iconfont'),
  iconfontCss = require('gulp-iconfont-css'),
  config      = require('../config').iconfont;



gulp.task('iconfont', function(){
  gulp.src([config.src])
    .pipe(iconfontCss({
      fontName: config.fontName,
      path: config.path,
      targetPath: config.targetPath,
      fontPath: config.fontPath
    }))
    .pipe(iconfont({
      fontName: config.fontName,
      normalize: true
     }))
    .pipe(gulp.dest(config.dest));
});