var uglify = require('gulp-uglify');
var gulp   = require('gulp');
var config = require('../config').uglify;

gulp.task('uglify', function() {
  gulp.src(config.src)
    .pipe(gulp.dest(config.dest))
});
gulp.task('uglify:build', function() {
  gulp.src(config.src)
    .pipe(uglify(), {
      outSourceMap: true
    })
    .pipe(gulp.dest(config.dest))
});