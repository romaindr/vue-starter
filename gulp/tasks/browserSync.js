  var browserSync = require('browser-sync');

var gulp        = require('gulp');
var config      = require('../config').browserSync;
var dest      = require('../config').dest;

gulp.task('browserSync', ['build'], function() {
  browserSync(config);
  gulp.watch([dest+'/**/*.*'], browserSync.reload);
});
