var gulp   = require('gulp');

var config   = require('../config').datas;

gulp.task('datas', function (cb) {
    return gulp.src(config.src)
    .pipe(gulp.dest(config.dest))
});