'use strict';

var sizes = require('./../../../utils/app');
module.exports =  {
  template: require('./contact.html'),
  route: {
    id: 'contact',
    path: '/contact'
  },
  data: function(){

  },
  methods: {
    resize: function(){
      console.log(sizes.win_w);
    }
  },
  ready: function() {
    console.log(this.$data.data.home)

  },
  created:function(){

  },
  attached: function() {
    this.$dispatch('child-attached', this);
  },
  beforeDestroy: function() {

  }
};
