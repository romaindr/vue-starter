'use strict';

var sizes = require('./../../../utils/app');

module.exports = {
  template: require('./navigation.html'),
  data: function(){
    return {}
  },
  methods:{
    init: function () {
      this.navigation = $(this.$el);
    },
    resize: function () {

    },
  },

  ready: function() {
    this.init();
  },
  attached: function(){
  }
};
