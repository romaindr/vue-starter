'use strict';

var sizes = module.exports = {
  win_w: $(window).width(),
  win_h: $(window).height(),

  onResize: function () {
    this.win_w = $(window).width();
    this.win_h = $(window).height();
  }
};
