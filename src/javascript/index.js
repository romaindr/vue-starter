'use strict';

require('./imports');

var Vue = require('vue'),
    router = require('./router'),
    request = require('superagent'),
    sizes = require('./utils/app');

/*
    Plugins, lib config...
 */

function loadData(){
  request.get('/assets/datas/data.json').end(init);
}
function init(res) {
  console.log(res);
  window.app = new Vue({
    el: 'body',
    data:function(){
      return {
        context: {}, // reference to the router context
        content: res.body,
        currentPage: null
      }
    },
    components: {
      /* LAYOUT */
      'header': require('./views/layout/header/header'),
      'navigation': require('./views/layout/navigation/navigation'),
      'footer': require('./views/layout/footer/footer'),
      /* PAGES */
      'home': require('./views/sections/home/home'),
      'contact': require('./views/sections/contact/contact')
    },
    ready: function() {
      this.$on('child-attached', this.onChildAttached.bind(this));
      router.on('router:update', this.onRouteUpdate.bind(this));
      router.addRoute(require('./views/sections/home/home').route);
      router.addRoute(require('./views/sections/contact/contact').route);
      router.setDefaultRoute('home');


      this.resizableComponents = [];
      //
      $(window).on('resize', this.resize.bind(this));

    },
    methods: {
      onRouteUpdate: function(context) {
        this.context = context
        this.currentPage = this.context.id;

        this.$root.$emit('$route.update', this.currentPage);

      },
      onChildAttached: function(child){
        console.debug('[index] onChildAttached');

        this.resizableComponents.push(this.$.page);
        this.resize();


      },
      onLoaded: function(){

      },
      resize: function(){
        if($(window).width() === sizes.win_w && $(window).height() === sizes.win_h ) return;
        sizes.onResize();
        var components = this.resizableComponents;

        for (var c in components) {
          components[c].resize();
        }
      }
    }
  });
}

window.onload = loadData;
