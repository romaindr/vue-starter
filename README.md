# README #

Front-end delivery for ***.

### How do I get set up? ###

* Gulp project
* Vue.js (framework JS)

1) How to install after cloning:
```
#!bash
npm install
```

2) Create a VHOST local
```
#!bash
path-to-project/web/
```

3) set your VHOST in the gulp config file
```
#!bash
proxy: "***.***"

```


4) To start the server:
```
#!bash
gulp watch
```